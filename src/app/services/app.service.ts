import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }


  login(formBody: object): Observable<any> {
    return this.http.post(
      "https://devgroceryapi.spericorn.com/api/auth/login",
      formBody);
  }

  register(formBody: object): Observable<any> {
    return this.http.post(
      "https://devgroceryapi.spericorn.com/api/auth/register",
      formBody);
  }

  profile(): Observable<any> {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }
    return this.http.get(
      "https://devgroceryapi.spericorn.com/api/user",
      httpOptions);
  }

  emailValidation(formBody: object): Observable<any> {
    return this.http.post(
      "https://devgroceryapi.spericorn.com/api/auth/checkMail",
      formBody);
  }
}
