import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {

  strEmail: any = '';
  strPassword: any = '';
  strUsername: any = '';
  intPhNum: any = '';
  strConfirmPwsd: any = '';

  constructor(private service: AppService, private router: Router) { }

  ngOnInit(): void {
  }

  onClickRegister() {
    if (this.validateReg()) {
      const frmbody = { email: this.strEmail }
      this.service.emailValidation(frmbody).subscribe(response => {
        if (response.success) {
          this.postRegister();
        } else {
          alert(response.message);
        }
      })

    }
  }

  postRegister() {
    const formBody = {
      "email": this.strEmail,
      "password": this.strPassword,
      "username": this.strUsername,
      "phone": this.intPhNum
    }
    this.service.register(formBody).subscribe(response => {
      console.log(response)
      if (response.success) {
        localStorage.setItem('token', response.data.token);
        alert('Registered successfully')
        this.router.navigate(['user/Login']);
      } else {
        alert(response.message);
      }
    }, error => {
      console.log(error);
    });
  }

  validateReg() {
    const format = /[!~#$%@^&+*()\=\[\]{};':`"\\|,<>\/?]/;
    if (!this.strEmail.trim()) {
      alert('Email cannot be empty');
      return false
    } else if (!this.strUsername.trim()) {
      alert('Username cannot be empty');
      return false
    } else if (!this.strPassword.trim()) {
      alert('Password cannot be empty');
      return false
    } else if (!this.intPhNum.trim()) {
      alert('Phone number cannot be empty');
      return false
    } else if (!this.strConfirmPwsd.trim()) {
      alert('Confirm password cannot be empty');
      return false
    }

    if (this.strPassword.length < 8) {
      alert('Password should contain atleast 8 character')
      return false;
    }

    let data = this.strPassword.match(format);
    console.log(data)
    if (data == null) {
      alert('Password should contain atleast one special character')
      return false;
    }
    // if(format. test(this.strPassword)){
    //   return true;
    //   } else {
    //   return false;
    //   }

    if (this.strConfirmPwsd.trim() != this.strPassword.trim()) {
      alert('Password missmatch')
      return false
    }
    return true
  }

}
