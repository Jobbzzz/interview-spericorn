import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  username: string = "";
  password: string = "";
  constructor(private service: AppService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    //add validation

    this.service.login({
      "email": this.username,
      "password": this.password
    }).subscribe(response => {
      if (response.success) {
        localStorage.setItem('token', response.data.token);
        this.router.navigate(['user/profile']);
      } else {
        alert(response.message)
      }
    }, error => {
      console.log(error);
    })
  }

}
