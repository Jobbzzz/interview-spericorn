import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: any = null;
  strUserName: any = '';
  intPhNum: any;
  strEmail: any;
  constructor(private service: AppService, private router: Router) { }

  ngOnInit(): void {
    this.service.profile().subscribe(response => {
      this.strUserName = response.data.userData.username;
      this.strEmail = response.data.userData.email;
      this.intPhNum = response.data.userData.phone;
      console.log(this.strUserName)
    }, error => {
      console.log(error);
    })
  }

  onClickLogOut() {
    localStorage.clear();
    this.router.navigate(['user/Login']);
  }

}
